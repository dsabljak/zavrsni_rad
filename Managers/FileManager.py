from netCDF4 import *
import matplotlib.pyplot as plt

from Analyser.Analyser import Analyser
from Analyser.NameAnalyser import NameAnalyzer
from DataHolders.S3DataHolders.Sentinel3Product import Sentinel3Product

from DataHolders.S5PDataHolders.Sentinel5PProduct import Sentinel5PProduct
from DataHolders.S3DataHolders.SeaSurfaceTemperature import SeaSurfaceTemperature
from seaborn import heatmap

##/home/dsabljak/Desktop/Copernicus_materijali/20191009195549-MAR-L2P_GHRSST-SSTskin-SLSTRA-20191009221405-v02.0-fv01.0.nc



class FileManager:
    def __init__(self, path):
       # path = 'test data/S3B_SL_2_WST____20200205T071414_20200205T071714_20200205T082256_0179_035_149_0540_MAR_O_NR_003.SEN3/20200205071327-MAR-L2P_GHRSST-SSTskin-SLSTRB-20200205082101-v02.0-fv01.0.nc'
       # path2 = 'test data/S3B_SL_2_WST____20200205T175010_20200205T175310_20200205T195104_0179_035_155_2340_MAR_O_NR_003.SEN3/20200205174559-MAR-L2P_GHRSST-SSTskin-SLSTRB-20200205194915-v02.0-fv01.0.nc'
       # path4 = 'test data/S5P_NRTI_L2__CO_____20200223T221859_20200223T222359_12251_01_010302_20200223T225123.nc'
       # self.file = Dataset(path4, 'r')
        self.path = path

        name = NameAnalyzer(self.path)
        try:
            if name.is_s5p():
                self.data = Sentinel5PProduct(self.path)
            elif name.is_s3():
                self.data = Sentinel3Product(self.path)
            else:
                raise Exception("Unsupported product")
            print('tu')
        except:
            raise Exception("Unsupported product")

        self.product = self.data.get_product()
        self.workable_data = self.product.get_data()
        self.lat = self.product.get_lat()
        self.lon = self.product.get_lon()


        #self.a=np.array(self.file.groups['PRODUCT']['carbonmonoxide_total_column'][0])
        #heatmap(a)
        #print(a)
        #for i in self.file.groups:
        #    print(i)

        #print(self.file['sea_surface_temperature'][0])


    #    for i in self.file['sea_surface_temperature'][0][:][0]:
     #       print(i)

        #print(f'DUljina 3D: {len(self.file["sea_surface_temperature"][0])}')
       # self.start_analysis()
        #for i in range(len(self.file["sea_surface_temperature"][0])):
        #for j in range(len(self.file["sea_surface_temperature"][0][0])):
        #    print(self.file["sea_surface_temperature"][0][5][j], file=open('proba.txt', 'a'))
        ##Velicina polja je 1 1200 1500


    def make_graph(self):

        mini = self.product.get_min_value()
        maxi = self.product.get_max_value()
        heatmap(self.workable_data, vmin=mini, vmax=maxi)
        plt.show(block=True)
        plt.interactive(False)
        return

    def get_coord_len_x(self):
        return len(self.lat)

    def get_coord_len_y(self):
        return len(self.lat[0])

    def get_lat_unit(self):
        return self.product.get_lat_unit()

    def get_lon_unit(self):
        return self.product.get_lon_unit()

    def get_lat(self, x, y):
        return self.lat[x][y]

    def get_lon(self, x, y):
        return self.lon[x][y]

    def get_value(self, x, y):
        return self.workable_data[x][y]

    def get_average(self):
        return self.product.get_average()

    def get_unit(self):
        return self.product.get_unit()

    def get_name(self):
        return self.product.get_name()

    def get_time(self):
        return self.product.get_time()

    def start_analysis(self, min_value, max_value):
        print("Pocinje analiza")
     #   workable_data = self.data.get_product().get_data()
      #  mini = self.data.get_product().get_min_value()
      #  maxi = self.data.get_product().get_max_value()


        #data = temperature_data.get_data()[0]
        #dataa = temperature_data.get_data()
        # for i in range(len(data)):
        #     for j in range(len(data[0])):
        #         if data[i][j] < 200:
        #             print(data[i][j], i, j )
        #ma = temperature_data.get_max_value()
        #mi = temperature_data.get_min_value()

        #heatmap(self.workable_data, vmin=mini, vmax=maxi)


       # plt.contourf(data)
     #   avg = temperature_data.get_average()
      #  print(avg)

      #  variation = temperature_data.get_variation()



        # # print(variation, file = open('proba.txt','w'))
       # fig = plt.figure()
        # # ax = plt.axes(projection='3d')
        # # ax.scatter3D(x, y, variation)
        # #j=[]
        # #j.append(variation)
        # x = np.linspace(0,1200,1200)
        # y = np.linspace(0,1500,1500)
        # x_1, y_1 = np.meshgrid(x, y)

        #plt.contourf(variation)
        #plt.colorbar()
        #heatmap(variation)
     #   plt.show(block=True)
     #   plt.interactive(False)
        analizator = Analyser(self.workable_data)

        return analizator.analyse(min_value, max_value)
        # print('Analiza gotova')

    def __repr__(self):
        return self.path

#FileManager('a')
