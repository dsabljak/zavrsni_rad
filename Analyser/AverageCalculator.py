
class AverageCalculator:

    def calculate_average(data, x, y):
        counter = 0
        avg = 0
        for i in range(x):
            for j in range(y):
                if data[i][j] - 0 == 0:
                    continue
                else:
                    counter += 1
                    # print(f'data  {data[0][i][j]}')
                    avg += data[i][j]
        avg /= counter
        return avg