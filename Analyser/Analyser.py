class Analyser():
    def __init__(self, data_array):
        self.data_array = data_array



    def analyse(self, perc_diff_min, perc_diff_max):
        anomalys = []

        for i in range(1, len(self.data_array)-1, 2):
            for j in range(1, len(self.data_array[0])-1, 2):
                if abs(self.calculate_percentage_difference(i,j, i-1, j-1)) > perc_diff_min and abs(self.calculate_percentage_difference(i,j, i-1, j-1)) < perc_diff_max:
                    diff = self.calculate_difference(i, j, i-1, j-1)
                    anomalys.append((i, j, abs(diff), i-1, j-1))

                elif abs(self.calculate_percentage_difference(i,j, i-1, j)) > perc_diff_min and abs(self.calculate_percentage_difference(i,j, i-1, j)) < perc_diff_max:
                    diff = self.calculate_difference(i, j, i-1, j)
                    anomalys.append((i, j, abs(diff), i-1, j))

                elif abs(self.calculate_percentage_difference(i,j, i-1, j+1)) > perc_diff_min and abs(self.calculate_percentage_difference(i,j, i-1, j+1)) < perc_diff_max:
                    diff = self.calculate_difference(i, j, i-1, j+1)
                    anomalys.append((i, j, abs(diff), i-1, j+1))

                elif abs(self.calculate_percentage_difference(i,j, i, j-1)) > perc_diff_min and abs(self.calculate_percentage_difference(i,j, i, j-1)) < perc_diff_max :
                    diff = self.calculate_difference(i, j, i, j-1)
                    anomalys.append((i, j, abs(diff), i, j-1))

                elif abs(self.calculate_percentage_difference(i,j, i, j+1)) > perc_diff_min and abs(self.calculate_percentage_difference(i,j, i, j+1)) < perc_diff_max:
                    diff = self.calculate_difference(i, j, i, j+1)
                    anomalys.append((i, j, abs(diff), i, j+1))

                elif abs(self.calculate_percentage_difference(i,j, i+1, j-1)) > perc_diff_min and abs(self.calculate_percentage_difference(i,j, i+1, j-1)) < perc_diff_max:
                    diff = self.calculate_difference(i, j, i+1, j-1)
                    anomalys.append((i, j, abs(diff), i+1, j-1))

                elif abs(self.calculate_percentage_difference(i,j, i+1, j)) > perc_diff_min and abs(self.calculate_percentage_difference(i,j, i+1, j)) < perc_diff_max:
                    diff = self.calculate_difference(i, j, i+1, j)
                    anomalys.append((i, j, abs(diff), i+1, j))

                elif abs(self.calculate_percentage_difference(i,j, i+1, j+1)) > perc_diff_min and abs(self.calculate_percentage_difference(i,j, i+1, j+1)) < perc_diff_max:
                    diff = self.calculate_difference(i, j, i+1, j+1)
                    anomalys.append((i, j, abs(diff), i+1, j+1))

        return anomalys
    
    def calculate_percentage_difference(self, x, y, other_x, other_y):
        if self.data_array[x][y] == 0 or self.data_array[other_x][other_y] == 0:
            return 0
        return abs(200*(self.data_array[x][y]-self.data_array[other_x][other_y])/(self.data_array[x][y]+self.data_array[other_x][other_y]))

    def calculate_difference(self, x, y, other_x, other_y):
        return(abs(self.data_array[x][y]-self.data_array[other_x][other_y]))