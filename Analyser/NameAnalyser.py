

### Class with methods for checking which sentinel data is given

### TODO think better way for S3

class NameAnalyzer:

    def __init__(self, name):
        self.name = name

    def is_s5p(self):
        return 'S5P_' in self.name

    def is_s3(self):
        return '-MAR-L2P_GHRSST-SSTskin-SLSTR' in self.name

