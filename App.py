from concurrent.futures.thread import ThreadPoolExecutor
from multiprocessing.pool import ThreadPool
from queue import Queue
from threading import Thread
from tkinter import *
from tkinter import filedialog
from tkinter import messagebox
from tkinter.tix import PanedWindow
from tkinter.ttk import Separator


from Managers.FileManager import FileManager
import os
import threading

class App(Frame):
    def __init__(self, root):
        super().__init__(root)
        self.R = root
        self.R.title("App")
        self.grid(row=5, column=2)
        self.file = ''
        self.file_name = ''
        self.createGUI()

    def createGUI(self):

        self.open_file_button = Button(text='Open file', command=self.askforfile)
        self.open_file_button.grid(row=0, column=0)

        self.analyze_button = Button(text='Analyze', command=self.analyze)
        self.analyze_button.grid(row=0, column=1)

        self.text_log = Text(bg='black', fg='white')
        self.text_log.grid(row=1, column=0, rowspan=5, columnspan=4)

        self.average_button = Button(text="Calculate average value", command=self.calculate_average)
        self.average_button.grid(row=0, column=2)

        self.draw_graph = Button(text="Show graph", command=self.get_graph)
        self.draw_graph.grid(row=0, column=3)

        self.min_perc = StringVar()
        self.max_perc = StringVar()

        Label(text="Anomally settings").grid(row=1, column=6, columnspan=2)
        Label(text="Min percentage diff:").grid(row=2, column=6)
        Entry(textvariable=self.min_perc).grid(row=2, column=7)

        Label(text="Max percentage diff:").grid(row=3, column=6)
        Entry(textvariable=self.max_perc).grid(row=3, column=7)

        self.anomaly_button = Button(text="Look for anomaly", command=self.get_anomaly)
        self.anomaly_button.grid(row=4, column=6, columnspan=2)

        Separator(orient=VERTICAL).grid(row=0, column=8, rowspan=6, sticky='ns')
        self.x_coord_user = StringVar()
        self.y_coord_user = StringVar()

        Label(text="Coordinate analysis").grid(row=1, column=9, columnspan=2)
        Label(text="X coordinate").grid(row=2, column=9)
        Entry(textvariable=self.x_coord_user).grid(row=2, column=10)
        Label(text="Y coordinate").grid(row=3, column=9)
        Entry(textvariable=self.y_coord_user).grid(row=3, column=10)

        self.coord_button = Button(text="Analyze coordinate", command=self.analyze_coord)
        self.coord_button.grid(row=4, column=9, columnspan=2)
        self.createMenu()


    def check_coord(self, x, y):
        return x.isdigit() and y.isdigit()

    def analyze_coord(self):
        if self.check_if_file_opened():
            return

        x_coord = self.x_coord_user.get()
        y_coord = self.y_coord_user.get()

        if not self.check_coord(x_coord, y_coord):
            messagebox.showerror("Error", "Coordinate entries must be numbers.")
            return

        x_coord = int(x_coord)
        y_coord = int(y_coord)
        try:
            lat = self.file.get_lat(x_coord, y_coord)
            lat_unit = self.file.get_lat_unit()
            lon_unit = self.file.get_lon_unit()
            lon = self.file.get_lon(x_coord, y_coord)
            value = self.file.get_value(x_coord, y_coord)
            unit = self.file.get_unit()

            self.text_log.insert(END, f"({x_coord}, {y_coord}) is ({lat} {lat_unit}, {lon} {lon_unit}) with value: {value} {unit}\n")

        except:
            messagebox.showerror("Error", "Not a valid coordinate.\nPress Analyze button to see valid coordinate range.")




    def check_params(self, mini, maxi):
        try:
            if mini == "":
                mini = 0
            if maxi == "":
                maxi = 1000000
            mini_float = float(mini)
            maxi_float = float(maxi)
            if mini_float > maxi_float:
                messagebox.showerror("Error", "Min value must be less than max value.")
                return False
        except:
            messagebox.showerror("Error", "Input must be numbers.")
            return False

        return True

    def write_on_log(self, res):
        result_list = res.result()
        if len(result_list)==0:
            self.text_log.insert(END, f"No anomalies found.\n")
        for i in result_list:
            x = i[0]
            y = i[1]
            diff = i[2]
            x_other = i[3]
            y_other = i[4]
            self.text_log.insert(END, f"Between ({x}, {y}) and ({x_other}, {y_other}) difference is {diff} {self.file.get_unit()}\n")

    def get_anomaly(self):
        if self.check_if_file_opened():
            return

        min_diff = self.min_perc.get()
        max_diff = self.max_perc.get()
        if not self.check_params(min_diff, max_diff):
            return

        if min_diff == "":
            min_diff = 0
        if max_diff == "":
            max_diff = 1000000

        print(min_diff, max_diff)

        self.text_log.insert(END, f"Looking for anomalies...\n")
        future = ThreadPoolExecutor().submit(self.file.start_analysis, float(min_diff), float(max_diff))
        future.add_done_callback(self.write_on_log)


        return

    def get_graph(self):
        if self.check_if_file_opened():
            return

        self.text_log.insert(END, f"Creating graph...\n")
        graph_thread = Thread(target=self.file.make_graph)
        graph_thread.start()

        return


    def average_threading(self):

        self.text_log.insert(END, f"Average is: {self.file.get_average()} {self.file.get_unit()}\n")
        

        return

    def calculate_average(self):
        if self.check_if_file_opened():
            return

        self.text_log.insert(END, f"Calculating average...\n")
        

        average_thread = Thread(target=self.average_threading)
        average_thread.start()

        return

    def analyze(self):
        if self.check_if_file_opened():
            return

        self.text_log.insert(END, f"Starting analysis for {self.file_name}\n")
        self.text_log.insert(END, f"Opened file is: {self.file.get_name()}\n")
        self.text_log.insert(END, f"Time of measurment: {self.file.get_time()}\n")
        self.text_log.insert(END, f"Valid coordinates are:\nX: 0-{self.file.get_coord_len_x()-1}\nY: 0-{self.file.get_coord_len_y()-1}\n")

            
        return


    def savelog(self):
        logfile = filedialog.asksaveasfile('w', defaultextension=".txt")
        if logfile is None:
            return
        else:
            logfile.write(self.text_log.get(1.0, END))
            logfile.close()



    def createMenu(self):
        menubar = Menu(self.R)

        filemenu = Menu(menubar)

        filemenu.add_command(label="Open file", command=self.askforfile)
        filemenu.add_command(label="Save file", command=self.savelog)
        filemenu.add_command(label="Exit", command=self.exit)


        menubar.add_cascade(label='File', menu=filemenu)

        helpmenu = Menu(menubar)
        helpmenu.add_command(label='Help', command=self.help)

        menubar.add_cascade(label='Help', menu=helpmenu)

        self.R.config(menu=menubar)

    def help(self):
        messagebox.showinfo("Help", "This app is made for analyzing data from Sentinel 3 and 5 satelites.\nYou can open Sentinel 3 or Sentinel 5P products. But not all products are currently available for analysis. ")

    def exit(self):
        exit = messagebox.askyesno("Exit", "Are You sure You want to exit program?")
        if exit:
            yes_no_cancel = messagebox.askyesnocancel("Save file", "Do You wish to save your log?")
            if yes_no_cancel is None:
                return
            elif yes_no_cancel:
                self.savelog()
                self.R.destroy()
            else:
                self.R.destroy()
            return
        else:
            return


    def askforfile(self):
        file = filedialog.askopenfilename()
        if file == '':
            return
        elif file[-3:len(file)] != '.nc':
            print("Invalid format")
            messagebox.showerror("Error", "Invalid format\n.nc format is needed")
        else:
            try:
                self.file = FileManager(file)
                self.file_name = os.path.split(file)[1]
                self.text_log.insert(END, f'Added {self.file_name}.\n')
                return

            except:
                messagebox.showerror("Error", "Unsupported product.\nPlease check list of supported products.")
                return


    def check_if_file_opened(self):
        if self.file == '':
            messagebox.showinfo("No file chosen", "Please open file.")
            return True
        return False

root = Tk()
root.resizable(False, False)
app = App(root)
root.mainloop()
