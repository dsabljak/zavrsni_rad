from datetime import datetime, timedelta

import numpy
from netCDF4._netCDF4 import Dataset

from Analyser.AverageCalculator import AverageCalculator


class SeaSurfaceTemperature:
    def __init__(self, dataset):
        self.time = datetime(1981, 1, 1, 0, 0, 0) + timedelta(
            seconds=int(numpy.array(dataset['time'][0].astype(int).reshape(-1))))
        self.data = numpy.array(dataset['sea_surface_temperature'][0])
        self.lat = numpy.array(dataset['lat'])
        self.lat_unit = dataset['lat'].units
        print(self.lat)
        self.lon = numpy.array(dataset['lon'])
        self.lon_unit = dataset['lon'].units
        self.x_len = len(self.data)
        self.y_len = len(self.data[0])
        self.clouds = numpy.array(dataset['l2p_flags'][0]) & 512
        self.reverse_clouds()
        self.filter_data()
        self.avg = 0
        self.variation = []
        self.name = dataset['sea_surface_temperature'].standard_name
        self.unit = dataset['sea_surface_temperature'].units

    def get_lat_unit(self):
        return self.lat_unit

    def get_lon_unit(self):
        return self.lon_unit
    def get_lat(self):
        return self.lat

    def get_lon(self):
        return self.lon

    def get_time(self):
        return self.time

    def get_name(self):
        return self.name

    def get_unit(self):
        return self.unit

    def get_data(self):
        self.filter_data()
        return self.data

    def filter_data(self):
        self.data[self.data < -273] = 0
        self.data *= self.clouds


    def get_average(self):
        # if self.avg != 0:
        #     return self.avg
        # counter = 0
        # for i in range(len(self.data[0])):
        #     for j in range(len(self.data[0][0])):
        #         if self.data[0][i][j] - 0 == 0:
        #             continue
        #         else:
        #             counter += 1
        #             #print(f'data  {self.data[0][i][j]}')
        #             self.avg += self.data[0][i][j]
        #
        # print(f'{counter} brojac')
        # print(f'{self.avg} prosjek suma')
        # print(f'{self.avg/counter} prosjek')
        # self.avg /= counter
        return AverageCalculator.calculate_average(self.get_data(), self.x_len, self.y_len)

    def get_variation(self):
        if len(self.variation) != 0:
            return self.variation
        print(f"Prosjek:  {self.avg}")
        self.variation_res =[]

        for i in range(len(self.data[0])):
            self.variation = []
            for j in range(len(self.data[0][0])):
                if self.data[0][i][j] - 0 == 0:
                    self.variation.append(0)
                else:
                    self.variation.append(self.data[0][i][j] - self.avg)

            self.variation_res.append(self.variation)

        return self.variation_res

    def get_min_value(self):
        return numpy.min(self.get_data()[numpy.nonzero(self.get_data())])

    def get_max_value(self):
        return numpy.amax(self.get_data())

    def reverse_clouds(self):
        self.clouds[self.clouds > 1] = 1
        self.clouds[self.clouds < 0] = 1
        self.clouds[self.clouds == 0] = 2
        self.clouds[self.clouds == 1] = 0
        self.clouds[self.clouds == 2] = 1

