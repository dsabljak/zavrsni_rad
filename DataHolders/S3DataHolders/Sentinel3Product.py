from netCDF4._netCDF4 import Dataset

from DataHolders.S3DataHolders.SeaSurfaceTemperature import SeaSurfaceTemperature


class Sentinel3Product:

    def __init__(self, path):
        self.path = path
        self.dataset = Dataset(self.path, 'r')

    def get_product(self):
        if 'SLSTR' in self.path:
            return SeaSurfaceTemperature(self.dataset)
