from _ssl import SSL_ERROR_WANT_X509_LOOKUP

from netCDF4._netCDF4 import Dataset

from Analyser.Analyser import Analyser

from DataHolders.S5PDataHolders.Sentinel5PDataHolder import Sentinel5PDataHolder


NO2 = "nitrogendioxide_tropospheric_column"
O3 = "ozone_total_vertical_column"
CO = "carbonmonoxide_total_column"
SO2 = "sulfurdioxide_total_vertical_column"
CH4 = "methane_mixing_ratio"
HCHO = "formaldehyde_tropospheric_vertical_column"

### AER_LH and AER_AI have more options so their data is not held by Sentinel5PDataHolder

class Sentinel5PProduct:
    def __init__(self, path):
        self.path = path
        if not self.check_path():
            raise Exception('Not Sentinel 5P product')

        self.dataset = Dataset(path, 'r')

    def check_path(self):
        if 'S5P_' not in self.path:
            return False
        return True

    def get_product(self):
        if '_SO2_' in self.path:
            return Sentinel5PDataHolder(self.dataset, SO2)
        elif '_O3_' in self.path:
            return Sentinel5PDataHolder(self.dataset, O3)
        elif '_NO2_' in self.path:
            return Sentinel5PDataHolder(self.dataset, NO2)
        elif '_HCHO_' in self.path:
            return Sentinel5PDataHolder(self.dataset, HCHO)
        elif '_CO_' in self.path:
            return Sentinel5PDataHolder(self.dataset, CO)
        elif '_CH4_' in self.path:
            return Sentinel5PDataHolder(self.dataset, CH4)

        else:
            raise Exception('Unsupported product :(')


#a = Sentinel5PProduct(
 #   'D:\Dominik\Fakultet\zavrsni_rad\\test data\S5P_NRTI_L2__O3_____20200228T222449_20200228T222949_12322_01_010107_20200228T230434.nc').get_product()
