import numpy as np
from datetime import datetime, timedelta

from seaborn import heatmap
import matplotlib.pyplot as plt

### Class for holding S5P data. Currently works for: SO2, CO, CH4, O3, HCHO, NO2
from Analyser.AverageCalculator import AverageCalculator


class Sentinel5PDataHolder:
    def __init__(self, dataset, product_keyword):
        self.name = product_keyword.split('_')[0]
        self.data = np.array(dataset.groups['PRODUCT'][product_keyword][0])
        self.x_len = len(np.array(dataset.groups['PRODUCT'][product_keyword][0]))
        self.y_len = len(np.array(dataset.groups['PRODUCT'][product_keyword][0][0]))
        self.unit = dataset.groups['PRODUCT'][product_keyword].units
        self.data_fill_value = dataset.groups['PRODUCT'][product_keyword]._FillValue
        self.lat = np.array(dataset.groups['PRODUCT']['latitude'][0])
        self.lat_unit = dataset.groups['PRODUCT']['latitude'].units
        self.lon = np.array(dataset.groups['PRODUCT']['longitude'][0])
        self.lon_unit = dataset.groups['PRODUCT']['longitude'].units
        self.qa_value = np.array(dataset.groups['PRODUCT']['qa_value'][0])
        self.time = datetime(2010, 1, 1, 0, 0, 0) + timedelta(
            seconds=int(np.array(dataset.groups['PRODUCT']['time'][0].astype(int).reshape(-1))))


        # data = self.get_data()
        # mi = self.get_min_value()
        # ma = self.get_max_value()
        # heatmap(data, vmin=mi, vmax=ma)
        # plt.show(block=True)
        # plt.interactive(False)

    def get_name(self):
        return self.name

    def get_data(self):
        self.filter_data()
        return self.data

    def get_lat(self):
        return self.lat

    def get_lon(self):
        return self.lon

    def get_lon_unit(self):
        return self.lon_unit

    def get_lat_unit(self):
        return self.lat_unit

    def get_qa_value(self):
        return self.qa_value

    def get_time(self):
        return self.time

    def filter_data(self):
        self.data[self.data == self.data_fill_value] = 0

    def get_min_value(self):
        return np.min(self.get_data()[np.nonzero(self.get_data())])

    def get_max_value(self):
        return np.amax(self.get_data())

    def get_average(self):
        return AverageCalculator.calculate_average(self.get_data(), self.x_len, self.y_len)

    def get_unit(self):
        return self.unit